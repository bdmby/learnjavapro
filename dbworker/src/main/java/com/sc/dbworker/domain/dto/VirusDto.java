package com.sc.dbworker.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class VirusDto {
    private String name;

    private LocalDate dateStart;

    private int incubationPeriod;
}
