package com.sc.dbworker.controller;

import com.sc.dbworker.domain.Virus;
import com.sc.dbworker.domain.dto.VirusDto;
import com.sc.dbworker.repository.VirusRepository;
import com.sc.dbworker.service.VirusService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/virus")
public class VirusController {
    private final VirusService virusService;

    @GetMapping(value = "/list")
    public List<Virus> list() {
        return virusService.list();
    }

    @GetMapping(value = "/get/{id}")
    public Virus getById(@PathVariable int id) {
        return virusService.getById(id);
    };

    @PostMapping
    public int save(@RequestBody Virus virus) {
        return virusService.save(virus);
    }

    @GetMapping(value = "/listWithIncubationPeriod")
    public List<VirusDto> listWithIncubationPeriod(@RequestParam("min") int min,
                                                   @RequestParam int max) {
        return virusService.listWithIncubationPeriod(min, max);
    }

    @GetMapping(value = "/listWithIncubationPeriod2")
    public List<Virus> listWithIncubationPeriod2(@RequestParam("min") int min,
                                                 @RequestParam int max) {
        return virusService.listWithIncubationPeriod2(min, max);
    }
}
