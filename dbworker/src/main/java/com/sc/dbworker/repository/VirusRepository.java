package com.sc.dbworker.repository;

import com.sc.dbworker.domain.Virus;
import com.sc.dbworker.domain.dto.VirusDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VirusRepository extends JpaRepository<Virus, Integer> {
    @Query("select new com.sc.dbworker.domain.dto.VirusDto(vr.name, vr.dateStart, vr.incubationPeriod) from Virus vr where vr.incubationPeriod between :min and :max")
    List<VirusDto> findByCondition(int min, int max);

    List<Virus> findAllByIncubationPeriodBetween(int min, int max);
}
