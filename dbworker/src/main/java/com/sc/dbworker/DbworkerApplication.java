package com.sc.dbworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbworkerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbworkerApplication.class, args);
    }

}
