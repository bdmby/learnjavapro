package com.sc.dbworker.service;

import com.sc.dbworker.domain.Virus;
import com.sc.dbworker.domain.dto.VirusDto;
import com.sc.dbworker.repository.VirusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
@Transactional
public class VirusServiceImpl implements VirusService {
    private final VirusRepository virusRepository;

    @Override
    public Virus getById(int id) {
        Optional<Virus> optionalVirus = virusRepository.findById(id);

        if (!optionalVirus.isPresent()) {
            throw new RuntimeException("Entity not found");
        }

        return optionalVirus.get();
    }

    @Override
    public List<Virus> list() {
        return virusRepository.findAll();
    }

    @Override
    public int save(Virus virus) {
        return virusRepository.save(virus).getId();
    }

    @Override
    public List<VirusDto> listWithIncubationPeriod(int min, int max){
        return virusRepository.findByCondition(min, max);
    };

    public List<Virus> listWithIncubationPeriod2(int min, int max){
        return virusRepository.findAllByIncubationPeriodBetween(min, max);
    };

}
