package com.sc.dbworker.service;

import com.sc.dbworker.domain.Virus;
import com.sc.dbworker.domain.dto.VirusDto;

import java.util.List;

public interface VirusService {
    Virus getById(int id);
    List<Virus> list();
    int save(Virus virus);
    List<VirusDto> listWithIncubationPeriod(int min, int max);
    List<Virus> listWithIncubationPeriod2(int min, int max);
}
