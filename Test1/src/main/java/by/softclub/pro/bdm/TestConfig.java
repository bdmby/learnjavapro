package by.softclub.pro.bdm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("by.softclub.pro.bdm")
@PropertySource("classpath:application.properties")
public class TestConfig {
    @Bean
    public EURConvertor eurConvertorBean(){
        return new EURConvertor();
    }

    @Bean
    public RUBConvertor rubConvertorBean(){
        return new RUBConvertor();
    }

    @Bean
    public USDConvertor usdConvertor(){
        return new USDConvertor();
    }
}
