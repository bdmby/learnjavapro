package by.softclub.pro.bdm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CurrencyExchange {
    public EURConvertor getEurConvertor() {
        return eurConvertor;
    }

    @Autowired
    public void setEurConvertor(EURConvertor eurConvertor) {
        this.eurConvertor = eurConvertor;
    }

    EURConvertor eurConvertor;

    public RUBConvertor getRubConvertor() {
        return rubConvertor;
    }

    public void setRubConvertor(RUBConvertor rubConvertor) {
        this.rubConvertor = rubConvertor;
    }

    @Autowired
    RUBConvertor rubConvertor;

    public USDConvertor getUsdConvertor() {
        return usdConvertor;
    }

    public void setUsdConvertor(USDConvertor usdConvertor) {
        this.usdConvertor = usdConvertor;
    }

    @Autowired
    USDConvertor usdConvertor;

    public int Exchange(Car car, Currency currency) {
        int price = 0;

        switch (currency){
            case EUR: {
                price = eurConvertor.getRate() * car.getPrice();
                break;
            }
            case RUB: {
                price = rubConvertor.getRate() * car.getPrice();
                break;
            }
            case USD: {
                price = usdConvertor.getRate() * car.getPrice();
                break;
            }
        }
        return price;
    }

    public CurrencyExchange(){

    }
}
