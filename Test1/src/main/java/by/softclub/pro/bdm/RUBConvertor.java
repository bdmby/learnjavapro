package by.softclub.pro.bdm;

import org.springframework.beans.factory.annotation.Value;

public class RUBConvertor {
    Currency currency;

    @Value("${rate.RUB}")
    int rate;

    RUBConvertor(){
        currency = Currency.RUB;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
