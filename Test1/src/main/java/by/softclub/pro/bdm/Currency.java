package by.softclub.pro.bdm;

public enum Currency {
    BYN ("BYN", 1),
    EUR ("EUR", 2),
    USD ("USD", 3),
    RUB ("RUB", 4);

    Currency(String iso, int rateBYN){
        this.iso = iso;
        this.rateBYN = rateBYN;
    }

    public int getRateBYN() {
        return rateBYN;
    }

    public void setRateBYN(int rateBYN) {
        this.rateBYN = rateBYN;
    }

    private int rateBYN;

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    String iso;
}
