package by.softclub.pro.bdm;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.FileWriter;
import java.io.IOException;

public class TestSpring1 {

    public static void main(String[] args) throws IOException {
       AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
               TestConfig.class
        );

        CurrencyExchange currencyExchange = context.getBean("currencyExchange", CurrencyExchange.class);

        Car car = Car.valueOf(args[0]);
        Currency currency = Currency.valueOf(args[1]);
        int price = currencyExchange.Exchange(car, currency);

        int len = args.length;
        if (len < 3){
            System.out.println(price);
        }
        else {
            String fileName = args[2];

            FileWriter writer = new FileWriter(fileName.toString(), false);

            writer.write(String.valueOf(price));
            writer.append('\n');

            writer.flush();
        }

        context.close();

    }
}
