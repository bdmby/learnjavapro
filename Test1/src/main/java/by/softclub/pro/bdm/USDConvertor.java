package by.softclub.pro.bdm;

import org.springframework.beans.factory.annotation.Value;

public class USDConvertor {
    Currency currency;

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Value("${rate.USD}")
    int rate;

    USDConvertor(){
        currency = Currency.USD;
    }
}
