package by.softclub.pro.bdm;

public enum Car {
    LADA ("LADA",10),
    MERSEDES ("MERSEDES", 20),
    SKODA ("SKODA", 30),
    VW ("VW", 40);

    Car (String name, int price)
    {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    private int price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
