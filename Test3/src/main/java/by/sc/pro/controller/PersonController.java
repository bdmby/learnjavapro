package by.sc.pro.controller;

import by.sc.pro.domain.Person;
import by.sc.pro.service.PersonService;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    private List<String> tasks = Arrays.asList("a", "b", "c", "d", "e", "f", "g");

    @GetMapping("/list")
    public String getList(Model model) {
        model.addAttribute("serverTime", new Date());
        model.addAttribute("people", personService.getList());
        model.addAttribute("person", new Person());
        return "index";
    }

    @GetMapping("/welcome")
    public String welcome(Model model) {
        model.addAttribute("message", "Softclub");
        model.addAttribute("tasks", tasks);

        return "welcome"; //view
    }

    @PostMapping
    public String addPerson(@ModelAttribute(value = "person") Person person, Model model) {
        personService.add(person);
        model.addAttribute("serverTime", new Date());
        model.addAttribute("people", personService.getList());
        model.addAttribute("person", new Person());
        return "index";
    }
}
