package by.sc.pro.service;

import by.sc.pro.domain.Person;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    private final List<Person> list = new ArrayList<>();

    @PostConstruct
    public void init() {
        list.add(new Person(1, "John", 32, "images/test.png"));
        list.add(new Person(2, "Alex", 22, "images/test.png"));
        list.add(new Person(3, "Max", 25, "images/test.png"));
        list.add(new Person(4, "Jack", 37, "images/test.png"));
    }

    @Override
    public List<Person> getList() {
        return this.list;
    }

    @Override
    public void add(Person person) {
        if (person.getId() == 0) {
          person.setId(list.size() + 1);
        }

        list.add(person);
    }
}
