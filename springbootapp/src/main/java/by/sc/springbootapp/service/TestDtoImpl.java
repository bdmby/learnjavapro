package by.sc.springbootapp.service;

import by.sc.springbootapp.domain.Cars;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;
import java.util.List;

@Service
public class TestDtoImpl implements TestDto {
    private List<Cars> cars;

    @PostMapping
    public void init(){
        cars.add(
                Cars.builder()
                .engine("gas")
                .volume(BigDecimal.valueOf(1.8))
                .build()
        );
    }

    @Override
    public List<Cars> getList() {
        return cars;
    }
}