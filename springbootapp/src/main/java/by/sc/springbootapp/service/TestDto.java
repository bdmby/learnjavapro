package by.sc.springbootapp.service;

import by.sc.springbootapp.domain.Cars;
import java.util.List;

public interface TestDto {
    public List<Cars> getList();
}
