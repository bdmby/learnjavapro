package by.sc.springbootapp.controller;

import by.sc.springbootapp.dto.SimpleObjectDto;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {
    @GetMapping(value = "/hello")
    public SimpleObjectDto hello(@RequestParam Integer id,
                        @RequestParam String name) {
        SimpleObjectDto dto = new SimpleObjectDto();
        dto.setId(id);
        dto.setName(name);
        return dto;
    }

    @PostMapping(value = "/hello2")
    public SimpleObjectDto hello2(@RequestBody SimpleObjectDto dto) {
        dto.setId(null);
        dto.setName("dlkfgjkdlfjglkdejfgkljdekl");
        return dto;
    }

    @GetMapping(value = "/hello3/{id3}")
    public SimpleObjectDto hello3(@PathVariable(value = "id3") Integer id) {
        SimpleObjectDto dto = new SimpleObjectDto();
        dto.setId(id);
        return dto;
    }

    @GetMapping(value = "/hello4/{id}")
    public SimpleObjectDto hello4(@PathVariable Integer id) {
        SimpleObjectDto dto = new SimpleObjectDto();
        dto.setId(id);
        return dto;
    }
}
