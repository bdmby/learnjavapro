package by.sc.springbootapp.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
public class Cars {
    private String engine;
    private BigDecimal volume;
}
