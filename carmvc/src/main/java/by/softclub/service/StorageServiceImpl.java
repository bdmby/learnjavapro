package by.softclub.service;

import by.softclub.domain.Car;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StorageServiceImpl implements StorageService {
    private List<Car> cars = new ArrayList<>();

    @Override
    public void add(Car object) {
        this.cars.add(object);
    }

    @Override
    public Car getById(int objectId){
        return this.cars.stream()
                .filter(car -> car.getId() == objectId)
                .findFirst()
                .orElseThrow();
    }

    @Override
    public void deleteById(int objectId) {
        cars.removeIf(
                element -> element.getId() == objectId
        );
    }

    @Override
    public List<Car> getList() {
        return this.cars;
    }
}
