package by.softclub.service;

import by.softclub.domain.Car;

public interface StorageService extends BaseOperationService<Car> {

}
