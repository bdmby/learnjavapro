package by.softclub.service;

import by.softclub.domain.Car;

import java.util.List;

public interface BaseOperationService<T> {
    void add(T object);
    T getById(int objectId);
    void deleteById(int objectId);
    List<T> getList();
}
