package by.softclub.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("agro")
@Service
public class TestAgroImpl implements TestService {
    @Override
    public String getInfo() {
        return "Agro";
    }
}
