package by.softclub.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("mtb")
@Service
public class TestMTBImpl implements TestService {

    @Override
    public String getInfo() {
        return "MTB";
    }
}
