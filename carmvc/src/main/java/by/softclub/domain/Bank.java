package by.softclub.domain;

import lombok.Data;

@Data
public class Bank {
    private String name;
}
