package by.softclub.controller;

import by.softclub.domain.Car;
import by.softclub.service.HistoryService;
import by.softclub.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class StorageController {
    @Autowired
    private StorageService storageService;

    @Autowired
    private HistoryService historyService;

    @GetMapping(value = "/storage/list")
    public String storageList(Model model)
    {
        model.addAttribute("cars", storageService.getList());

        return "storage";
    }

    @PostMapping(value = "/buy")
    public String buyCar(@RequestParam("car") int id, Model model) {
        Car car = storageService.getById(id);
        storageService.deleteById(id);
        historyService.add(car);

        model.addAttribute("cars", storageService.getList());

        return "storage";
    }
}
