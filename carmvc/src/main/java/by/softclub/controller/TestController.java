package by.softclub.controller;

import by.softclub.domain.Bank;
import by.softclub.domain.Car;
import by.softclub.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    private TestService testService;

    @GetMapping(value = "/bank")
    public Bank getBank(){
        Bank bank = new Bank();

        bank.setName(testService.getInfo());

        return bank;
    }
}
