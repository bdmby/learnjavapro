package by.softclub.controller;

import by.softclub.domain.Car;
import by.softclub.service.CarService;
import by.softclub.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
public class CarController {
    @Autowired
    private CarService carService;

    @Autowired
    private StorageService storageService;

    @GetMapping(value = "/car/list")
    public String carList(Model model) {
        model.addAttribute("cars", carService.getList());
        return "car";
    }

    @PostMapping(value = "/car")
    public String addCarInStorage(@RequestParam("car") int id, Model model) {
        //1: get car
        Car car = carService.getById(id);

        //2: delete card from cars in car view
        carService.deleteById(id);

        //3: put car in storage
        storageService.add(car);

        //4: get list from car view
        List<Car> cars = carService.getList();

        model.addAttribute("cars", cars);

        return "car";
    }
}
