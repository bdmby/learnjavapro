package by.softclub.controller;

import by.softclub.service.CarService;
import by.softclub.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HistoryController {
    @Autowired
    HistoryService historyService;

    @GetMapping(value = "/history/list")
    public String historyList(Model model) {
        //model.addAttribute("cars", historyService.getList());
        model.addAttribute("cars", historyService.getList());

        return "history";
    }

    @PostMapping(value = "/clear")
    public String clearHistoryList(Model model){

        return "history";
    }
}
