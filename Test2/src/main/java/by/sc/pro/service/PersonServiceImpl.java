package by.sc.pro.service;

import by.sc.pro.domain.Person;

import java.util.*;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    private final List<Person> list = new ArrayList<>();

    @PostConstruct
    public void init() {
        list.add(new Person(1, "John", 32));
        list.add(new Person(2, "Alex", 22));
        list.add(new Person(3, "Max", 25));
        list.add(new Person(4, "Jack", 37));

        Collections.sort(list);
    }

    @Override
    public List<Person> getList() {
        return this.list;
    }

    @Override
    public void add(Person person) {
        boolean findPerson = false;

        for (Person listPerson: list) {
            if (person.getId() == listPerson.getId()) {
                findPerson = true;
                break;
            }
        }

        if (!findPerson) {
            list.add(person);
        }

        Collections.sort(list);
    }
}
