package by.sc.pro.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person implements Comparable<Person> {
    private int id;
    private String name;
    private int age;

    @Override
    public int compareTo(Person person){
        return -(this.age - person.age);
    }

    @Override
    public String toString(){
        return "[{id=" + this.id + "}{name=" + this.name + "}{age=" + this.age + "}]";
    }

    @Override
    public boolean equals(Object other){
        if (other == null) {
            return false;
        }

        if (this.getClass() != other.getClass()){
            return false;
        }

        Person otherPerson = (Person) other;
        if (this.id != otherPerson.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode(){
        return this.id;
    }
}
