package com.sc.javapro.db.service.impl;

import com.sc.javapro.db.domain.Customer;
import com.sc.javapro.db.repository.CustomerRepository;
import com.sc.javapro.db.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<Customer> getCustometList() {
        return customerRepository.findAll();
    }
}
