package com.sc.javapro.db.service.impl;

import com.sc.javapro.db.domain.Account;
import com.sc.javapro.db.repository.AccountRepository;
import com.sc.javapro.db.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }
}
