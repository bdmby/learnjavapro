package com.sc.javapro.db.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "customer_id",
        foreignKey = @ForeignKey(value = ConstraintMode.CONSTRAINT,
            name = "customer_id_key"))
    @JsonBackReference
    private Customer customer;

    @Enumerated(value = EnumType.STRING)
    private AccountType accountType;
}
