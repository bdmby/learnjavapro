package com.sc.javapro.db.service;

import com.sc.javapro.db.domain.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getCustometList();
}
