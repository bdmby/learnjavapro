package com.sc.javapro.db.domain;

public enum AccountType {
    USD(840,"Доллары США"),
    EUR(978,"Евро"),
    BYN(933,"Белорусские рубли (новые)");

    private int id;
    private String name;

    AccountType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
