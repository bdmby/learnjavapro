package com.sc.javapro.db.service;

import com.sc.javapro.db.domain.Account;

import java.util.List;

public interface AccountService {
    List<Account> findAll();
}
