package com.sc.exchange;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sc.exchange.domain.Currency;
import com.sc.exchange.domain.Order;
import com.sc.exchange.domain.enums.OrderState;
import com.sc.exchange.dto.OrderDto;
import com.sc.exchange.repository.CurrencyRepository;
import com.sc.exchange.repository.OrderRepository;
import com.sc.exchange.service.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ExchangeApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    void addCurrencies() {
        var currency = new Currency();

        currency.setId(10);
        currency.setCode("TEST");
        currency.setDescription("TEST");

        currencyRepository.saveAndFlush(currency);

        currency.setCode("TEST IMMUTABLE");

        currencyRepository.saveAndFlush(currency);
    }

    @Test
    void addOrder() {
        var order = new Order();

        currencyRepository.findById(1).ifPresent(
                order::setSourceCurrency
        );

        currencyRepository.findById(2).ifPresent(
                order::setTargetCurrency
        );


        order.setAmount(new BigDecimal("100"));
        order.setFee(new BigDecimal("0.02"));

        order.setOrderState(OrderState.CREATED);

        orderRepository.saveAndFlush(order);

    }

    @Test
    void findOrders() {
        List<OrderDto> dtoList = orderService.findAll();
        Assertions.assertFalse(dtoList.isEmpty());
        System.out.println(dtoList);
    }

    @Test
    void findOrdersRest401() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/order")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized()).andReturn();

        String resultStr = result.getResponse().getContentAsString();
        assertNotNull(resultStr);
        System.out.println(resultStr);

    }

    @Test
    void findOrdersRest() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/order")
                .header("Authorization", "Basic YWRtaW46YWRtUGFzcw==")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        String resultStr = result.getResponse().getContentAsString();
        assertNotNull(resultStr);
        System.out.println(resultStr);

    }

    @Test
    void createOrdersRest() throws Exception {

        var dto = new OrderDto();

        dto.setTargetCurrency("USD");
        dto.setSourceCurrency("ETH");
        dto.setFee(BigDecimal.valueOf(23.09));
        dto.setAmount(BigDecimal.valueOf(100.67));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/order/add")
                .header("Authorization", "Basic YWRtaW46YWRtUGFzcw==")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

    }

    @Test
    void deleteOrdersRest() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.delete("/order/delete/1")
                .header("Authorization", "Basic dXNlcjI6dXNlcjJQYXNz")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    void generatePassword() throws Exception {
        String res = passwordEncoder.encode("qwerty");

        System.out.println(res);

        Assertions.assertTrue(passwordEncoder.matches("qwerty", res));
    }

}
