package com.sc.exchange.repository;

import com.sc.exchange.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsernameEquals(String username);
}
