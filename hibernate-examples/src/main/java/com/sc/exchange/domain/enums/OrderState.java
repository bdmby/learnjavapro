package com.sc.exchange.domain.enums;

public enum OrderState {
    CREATED,
    IN_PROCESS,
    FINISHED,
    ERROR;
}
