package com.sc.exchange.service;

import com.sc.exchange.dto.UserDto;

public interface UserService {
    void addUser(UserDto userDto);
}
