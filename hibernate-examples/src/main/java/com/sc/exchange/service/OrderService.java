package com.sc.exchange.service;

import com.sc.exchange.dto.OrderDto;

import java.util.List;

public interface OrderService {
    List<OrderDto> findAll();

    void save(OrderDto dto);

    void deleteById(int id);
}
