package com.sc.exchange.service;

import com.sc.exchange.domain.User;
import com.sc.exchange.dto.LoginDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthenticationService extends UserDetailsService {
    void replaceUserInCache(String token, User user);

    String login(User user, LoginDto loginDto);
}
