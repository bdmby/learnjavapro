package com.sc.exchange.service;

import com.sc.exchange.domain.Currency;
import com.sc.exchange.domain.Order;
import com.sc.exchange.dto.OrderDto;
import com.sc.exchange.repository.CurrencyRepository;
import com.sc.exchange.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = "orders")
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Cacheable
    @Override
    public List<OrderDto> findAll() {

        System.out.println(SecurityContextHolder.getContext().getAuthentication());

        return orderRepository.findAll().stream()
                .map(order -> {
                    var dto = new OrderDto();
                    dto.setAmount(order.getAmount());
                    dto.setFee(order.getFee());
                    dto.setId(order.getId());
                    dto.setSourceCurrency(order.getSourceCurrency().getCode());
                    dto.setTargetCurrency(order.getTargetCurrency().getCode());
                    return dto;
                }).collect(Collectors.toList());


    }

    @Override
    public void save(OrderDto dto) {
        var order = new Order();

        order.setAmount(dto.getAmount());
        order.setFee(dto.getFee());

        currencyRepository.findByCode(dto.getSourceCurrency()).ifPresent(
                order::setSourceCurrency
        );

        currencyRepository.findByCode(dto.getTargetCurrency()).ifPresent(
                order::setTargetCurrency
        );


        orderRepository.save(order);
    }

    @Override
    public void deleteById(int id) {
        orderRepository.deleteById(id);
    }
}
