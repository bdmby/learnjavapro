package com.sc.exchange.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginDto {
    private String login;
    private String password;
}
