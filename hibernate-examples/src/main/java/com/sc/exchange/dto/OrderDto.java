package com.sc.exchange.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Setter
@Getter
@ToString
@ApiModel(description = "Ордер на покупку/продажу криптовалюты")
public class OrderDto {

    private int id;

    @ApiModelProperty(notes = "Валюта продажи", required = true)
    private String sourceCurrency;

    @ApiModelProperty(notes = "Валюта покупки", required = true)
    private String targetCurrency;

    private BigDecimal amount;

    private BigDecimal fee;
}
