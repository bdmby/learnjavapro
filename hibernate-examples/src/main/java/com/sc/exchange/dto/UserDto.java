package com.sc.exchange.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@ApiModel(description = "Пользователь системы")
public class UserDto {
    private int id;

    @ApiModelProperty(notes = "Имя пользователя в системе", required = true)
    private String username;

    @ApiModelProperty(notes = "Пароль пользователя", required = true)
    private String password;

}
