package com.example.scexception.config;

import com.example.scexception.exception.FilmNotFoundException;
import com.example.scexception.service.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler  {
    @Autowired
    private TranslationService translationService;

    @ExceptionHandler(FilmNotFoundException.class)
    protected ResponseEntity<Object> handleFilmNotFoundException(FilmNotFoundException ex, WebRequest webRequest) {
        return handleExceptionInternal(ex, translationService.getLocalMessage(ex.getMessage(), webRequest.getLocale()),
                new HttpHeaders(), NOT_FOUND, webRequest);
    }
}
