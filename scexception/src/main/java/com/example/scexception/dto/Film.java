package com.example.scexception.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class Film {
    private int id;
    private String name;
    private String director;
    private int year;
}
