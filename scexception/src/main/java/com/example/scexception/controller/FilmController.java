package com.example.scexception.controller;

import com.example.scexception.dto.Film;
import com.example.scexception.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FilmController {

    @Autowired
    private FilmService filmService;

    @GetMapping("/list")
    public List<Film> getList() {
        return filmService.getList();
    }

    @GetMapping("/film/{id}")
    public Film getFilmById(@PathVariable int id) {
        return filmService.getFilmById(id);
    }
}
