package com.example.scexception.exception;

public class FilmNotFoundException extends RuntimeException {
    public static String error = "ERROR_FILM_NOT_FOUND";

    public FilmNotFoundException(String userError) {
        super(userError);
    }

    public FilmNotFoundException() {
        super(error);
    }
}
