package com.example.scexception.service;

import com.example.scexception.dto.Film;
import com.example.scexception.exception.FilmNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FilmServiceImpl implements FilmService {
    private List<Film> films = new ArrayList<>();

    @PostConstruct
    public void init() {
        films.add(new Film(1, "Pulp fiction", "Tarantino", 1992));
        films.add(new Film(2, "Fight Club", "Linch", 1999));
        films.add(new Film(3, "Mad Max", "Famost director", 1990));
    }

    @Override
    public List<Film> getList() {
        return films;
    }

    @Override
    public Film getFilmById(int id) {
        Optional<Film> film = films
                .stream().filter(item -> item.getId() == id)
                .findFirst();

        if (film.isEmpty()) {
            throw new FilmNotFoundException();
        }

        return film.get();
    }
}
