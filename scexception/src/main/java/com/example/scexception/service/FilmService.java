package com.example.scexception.service;

import com.example.scexception.dto.Film;
import java.util.List;

public interface FilmService {
    List<Film> getList();

    Film getFilmById(int id);
}
