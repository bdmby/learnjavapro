package com.example.scexception.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import java.util.Locale;

@Component
public class TranslationService {
    public static final Locale DEFAULT_LOCALE = Locale.ENGLISH;

    @Autowired
    private MessageSource messageSource;

    public String getLocalMessage(String code, Locale locale){
        if (locale == null) {
            locale = DEFAULT_LOCALE;
        }
        return messageSource.getMessage(code, null, locale);
    }
}
